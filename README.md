## Project Setup

sh
npm install


### Compile and Hot-Reload for Development

sh
npm run dev


### Compile and Minify for Production

sh
npm run build


### Changes
1. Refactor and Enhancement:
Converted Vue2 app to Vue3 app with the necessary migration of the code.
Some of the things changed for migration are as follows:
- Converted data property into Vue3 syntax as ref changed function style - Added computed property syntax 
- Optimised template and script functions
- Used new structure and syntax as per Vue3 
- Created manageable single functionality components and improved code readability

2. Pagination
Implemented pagination to show 100 rows per page for a better user experience.

3. Color Coding
Color codeed the table based on the "Status" column to make data interpretation easier.

4. Bonus Questions
Implement a search bar to filter rows based on specific criteria as follows:
Implimented search functionality as follows:
-- Search allows searching using the Status or searching using the Product name
-- Added key handler event on input and on change of that, it filters the data based on status or product name

5. Code Quality
- Used some old functions and code and changed some functionality for the new changes
- Added lint and formatter to remove any such errors
- Added router for navigation which can be used in future
- Optimised template and script functions
- Used new structure and syntax as per Vue3